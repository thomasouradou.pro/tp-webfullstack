const Movie = require('../models/movie');

exports.getAllMovies = async(req, res, next) => {
    try {
        const [allMovies] = await Movie.fetchAll();
        res.status(200).json(allMovies);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.postMovie = async(req, res, next) => {
    try {
        const postResponse = await Movie.post(req.body.name, req.body.author, req.body.release_date);
        res.status(201).json(postResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};



exports.deleteMovie = async(req, res, next) => {
    try {
        const deleteResponse = await Movie.delete(req.params.id);
        res.status(200).json(deleteResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};