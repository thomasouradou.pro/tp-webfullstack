const db = require('../util/database');

module.exports = class Movie {
    constructor(id, first_name, last_name, birth_date) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birth_date = birth_date
    }

    static fetchAll() {
        return db.execute('SELECT * FROM characters');
    }

    static post(first_name, last_name, birth_date) {
        return db.execute('INSERT INTO characters (first_name, last_name, birth_date) VALUES (?,?,?)', [first_name, last_name, birth_date]);
    }

    static delete(id) {
        return db.execute('DELETE FROM characters WHERE id = ?', [id]);
    }
};