const db = require('../util/database');

module.exports = class Movie {
    constructor(id, name, author, release_date) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.release_date = release_date
    }

    static fetchAll() {
        return db.execute('SELECT * FROM movies');
    }

    static post(name, author, release_date) {
        return db.execute('INSERT INTO movies (name, author, release_date) VALUES (?,?,?)', [name, author, release_date]);
    }

    static delete(id) {
        return db.execute('DELETE FROM movies WHERE id = ?', [id]);
    }
};