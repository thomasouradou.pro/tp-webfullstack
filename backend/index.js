const express = require('express');

const bodyParser = require('body-parser');

const movieRoutes = require('./routes/movie');
const characterRoutes = require('./routes/character');

const errorController = require('./controllers/error');
const { getAllMovies } = require('./controllers/movie');

const app = express();

const ports = process.env.PORT || 8080;



app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/movies', movieRoutes);
app.use('/characters', characterRoutes);
app.use(errorController.get404);

app.use(errorController.get500);

app.listen(ports, () => console.log(`listening on port ${ports}`));